
import { Col, Container, Input, Label, Row, Button } from "reactstrap";


const Registration = () =>{
    return(
        <Container>
            <h3 className="text-center text-success">REGISTRATION FORM</h3>
            <Row>
                <Col className="col-6">
                    <Row>
                        <Col className="col-4">
                            <Label>Firstname</Label>
                        </Col>
                        <Col className="col-8">
                            <Input />
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col className="col-4">
                            <Label>Lastname</Label>
                        </Col>
                        <Col className="col-8">
                            <Input />
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col className="col-4">
                            <Label>Birthday</Label>
                        </Col>
                        <Col className="col-8">
                            <Input />
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col className="col-4">
                            <Label>Gender</Label>
                        </Col>
                        <Col className="col-8">
                            <div className="d-inline">
                                <input type="radio" id="male" name="gender" value="Male" />
                                <label for="male">Male</label>
                            </div>

                            <div className="d-inline p-3">
                                <input type="radio" id="female" name="gender" value="Female" />
                                <label for="female">Female</label>
                            </div>
                        </Col>
                    </Row>
                </Col>
                <Col className="col-6">
                <Row>
                        <Col className="col-4">
                            <Label>Passport</Label>
                        </Col>
                        <Col className="col-8">
                            <Input />
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col className="col-4">
                            <Label>Email</Label>
                        </Col>
                        <Col className="col-8">
                            <Input />
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col className="col-4">
                            <Label>Country</Label>
                        </Col>
                        <Col className="col-8">
                            <Input />
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col className="col-4">
                            <Label>Region</Label>
                        </Col>
                        <Col className="col-8">
                            <Input />
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row className="mt-3">
                <Col className="col-2">
                    <Label>Subject:</Label>
                </Col>
                <Col className="col-10">
                    <textarea className="form-control" rows={8}></textarea>
                </Col>
            </Row>
            <Row className="d-flex justify-content-end mt-3"> 
                <Button className="" style={{width:"150px"}} color="success">Check Data</Button>.
                <Button className="ml-5" style={{width:"100px"}} color="success">Send</Button>
            </Row>
        </Container>
    )
}

export default Registration;